#importo nltk
import nltk

#este comando sirve para instalar los datos de nltk, solo se necesita la primera vez luego se puede eliminar
nltk.download()
#importar nltk.sentiment para poder usar el analizador de sentimientos
from nltk.sentiment import SentimentIntensityAnalyzer

#funcion para tokenizar texto
def tokenize_text(text):
	return nltk.word_tokenize(text)

#funcion para el reconocimiento de entidades
def recognize_entities(text):
    entities = nltk.chunk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(text)))
    return entities

#funcion para la clasificacion de dependencias
def tag_dependencies(text):
    tagged = nltk.pos_tag(nltk.word_tokenize(text))
    return tagged

#funcion para la clasificacion de gramatica
def classify_grammar(text):
    grammar = nltk.pos_tag(nltk.word_tokenize(text))
    return grammar

#funcion para analiza sentimientos
def analyze_sentiment(text):
    sid = SentimentIntensityAnalyzer()
    sentiment_score = sid.polarity_scores(text)
    return sentiment_score

#definir error en caso de que la entrada no sea valida
def handle_error():
    print("Error: Entrada no válida. Por favor, inténtelo de nuevo.")

def main():

#menu para seleccionar la herramienta deseada
    while True:
        print("\n--- Menú ---")
        print("1. Tokenización de Texto")
        print("2. Reconocimiento de Entidades")
        print("3. Etiquetador de Dependencias")
        print("4. Clasificación Gramatical")
        print("5. Clasificación de Sentimientos")
        print("6. Salir")

        choice = input("Seleccione una opción: ")

        if choice == "1":
            text = input("Ingrese el texto a tokenizar: ")
            print("Texto tokenizado:", tokenize_text(text))
        elif choice == "2":
            text = input("Ingrese el texto a analizar: ")
            print("Entidades reconocidas:", recognize_entities(text))
        elif choice == "3":
            text = input("Ingrese el texto a analizar: ")
            print("Dependencias etiquetadas:", tag_dependencies(text))
        elif choice == "4":
            text = input("Ingrese el texto a analizar: ")
            print("Clasificación gramatical:", classify_grammar(text))
        elif choice == "5":
            text = input("Ingrese el texto a analizar: ")
            print("Análisis de sentimientos:", analyze_sentiment(text))
        elif choice == "6":
            print("Saliendo del programa...")
            break
        else:
            handle_error()
main()