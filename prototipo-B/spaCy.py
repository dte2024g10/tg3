import spacy

# Función para tokenizar el texto
def tokenize_text(text):
    nlp = spacy.load("es_core_news_sm")  # Cargar el modelo de lenguaje en español
    doc = nlp(text)  # Procesar el texto con el modelo cargado
    tokens = [token.text for token in doc]  # Extraer el texto de cada token
    return tokens

# Función para clasificar gramaticalmente las palabras en el texto
def classify_pos(text):
    nlp = spacy.load("es_core_news_sm")  # Cargar el modelo de lenguaje en español
    doc = nlp(text)  # Procesar el texto con el modelo cargado
    pos_tags = [(token.text, token.pos_) for token in doc]  # Obtener el texto y la clasificación gramatical de cada token
    return pos_tags

# Función para reconocer entidades en el texto
def recognize_entities(text):
    nlp = spacy.load("es_core_news_sm")  # Cargar el modelo de lenguaje en español
    doc = nlp(text)  # Procesar el texto con el modelo cargado
    entities = [(ent.text, ent.label_) for ent in doc.ents]  # Obtener el texto y la etiqueta de cada entidad reconocida
    return entities

# Función para etiquetar dependencias en el texto
def dependency_tagger(text):
    nlp = spacy.load("es_core_news_sm")  # Cargar el modelo de lenguaje en español
    doc = nlp(text)  # Procesar el texto con el modelo cargado
    dependencies = [(token.text, token.dep_) for token in doc]  # Obtener el texto y la etiqueta de dependencia de cada token
    return dependencies

# Función para clasificar el sentimiento del texto
def classify_sentiment(text):
    # Aquí podrías implementar un análisis de sentimientos más sofisticado si lo deseas
    # Por ahora, simplemente retornaremos una clasificación basada en palabras clave
    positive_words = ["bueno", "feliz", "alegre", "contento"]
    negative_words = ["malo", "triste", "enojado", "decepcionado"]
    
    if any(word in text.lower() for word in positive_words):
        return "Positivo"
    elif any(word in text.lower() for word in negative_words):
        return "Negativo"
    else:
        return "Neutral"

# Función principal que maneja el menú y las opciones del usuario
def main():
    while True:
        print("\n--- Menú ---")
        print("1. Tokenización de texto")
        print("2. Clasificación Gramatical")
        print("3. Reconocimiento de Entidades")
        print("4. Etiquetador de Dependencias")
        print("5. Clasificación de Sentimientos")
        print("6. Salir")

        choice = input("Seleccione una opción: ")

        if choice == "1":
            text = input("Ingrese el texto a tokenizar: ")
            tokens = tokenize_text(text)
            print("Tokens:", tokens)
        elif choice == "2":
            text = input("Ingrese el texto a clasificar gramaticalmente: ")
            pos_tags = classify_pos(text)
            print("Clasificación gramatical:", pos_tags)
        elif choice == "3":
            text = input("Ingrese el texto para reconocimiento de entidades: ")
            entities = recognize_entities(text)
            print("Entidades:", entities)
        elif choice == "4":
            text = input("Ingrese el texto para etiquetado de dependencias: ")
            dependencies = dependency_tagger(text)
            print("Etiquetado de dependencias:", dependencies)
        elif choice == "5":
            text = input("Ingrese el texto para análisis de sentimientos: ")
            sentiment = classify_sentiment(text)
            print("Sentimiento:", sentiment)
        elif choice == "6":
            print("Saliendo del programa...")
            break
        else:
            print("Opción no válida. Por favor, seleccione una opción válida.")

# Lógica para ejecutar la función principal si este script es ejecutado directamente
if __name__ == "__main__":
    main()
